{%- macro action_metric(action_type, column_name) -%}

	    coalesce(
	    (
	        SELECT
	            SUM(actions.value['value']::numeric)
	        FROM
	            source,
	            lateral flatten (input=>source.actions) actions
	        WHERE actions.value['action_type'] = '{{ action_type }}'
	    ), 0.0) AS {{ column_name }}

{%- endmacro -%}
