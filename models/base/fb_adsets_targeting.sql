with source as (

    select * from {{var('schema')}}.adsets

),

renamed as (

    select

        nullif(id,'') as adset_id,

        name as name,

        -- Targeting definitions
        -- Adding them in case someone wants to analyze adsets
        -- for the same campaign by targeting type

		targeting['age_max'] as age_max,
		targeting['age_min'] as age_min,
		targeting['app_install_state'] as app_install_state,
		targeting['audience_network_positions'] as audience_network_positions,
		targeting['behaviors'] as behaviors,
		targeting['connections'] as connections,
		targeting['custom_audiences'] as custom_audiences,
		targeting['device_platforms'] as device_platforms,
		targeting['education_statuses'] as education_statuses,
		targeting['excluded_connections'] as excluded_connections,
		targeting['excluded_custom_audiences'] as excluded_custom_audiences,
		targeting['excluded_geo_locations']['cities'] as excluded_geo_locations__cities,
		targeting['excluded_geo_locations']['countries'] as excluded_geo_locations__countries,
		targeting['excluded_geo_locations']['country_groups'] as excluded_geo_locations__country_groups,
		targeting['excluded_geo_locations']['custom_locations'] as excluded_geo_locations__custom_locations,
		targeting['excluded_geo_locations']['geo_markets'] as excluded_geo_locations__geo_markets,
		targeting['excluded_geo_locations']['location_types'] as excluded_geo_locations__location_types,
		targeting['excluded_geo_locations']['regions'] as excluded_geo_locations__regions,
		targeting['excluded_geo_locations']['zips'] as excluded_geo_locations__zips,
		targeting['excluded_publisher_categories'] as excluded_publisher_categories,
		targeting['excluded_user_device'] as excluded_user_device,
		targeting['exclusions']['behaviors'] as exclusions__behaviors,
		targeting['exclusions']['connections'] as exclusions__connections,
		targeting['exclusions']['custom_audiences'] as exclusions__custom_audiences,
		targeting['exclusions']['education_majors'] as exclusions__education_majors,
		targeting['exclusions']['excluded_connections'] as exclusions__excluded_connections,
		targeting['exclusions']['excluded_custom_audiences'] as exclusions__excluded_custom_audiences,
		targeting['exclusions']['family_statuses'] as exclusions__family_statuses,
		targeting['exclusions']['friends_of_connections'] as exclusions__friends_of_connections,
		targeting['exclusions']['generation'] as exclusions__generation,
		targeting['exclusions']['home_ownership'] as exclusions__home_ownership,
		targeting['exclusions']['home_type'] as exclusions__home_type,
		targeting['exclusions']['household_composition'] as exclusions__household_composition,
		targeting['exclusions']['income'] as exclusions__income,
		targeting['exclusions']['industries'] as exclusions__industries,
		targeting['exclusions']['interests'] as exclusions__interests,
		targeting['exclusions']['life_events'] as exclusions__life_events,
		targeting['exclusions']['moms'] as exclusions__moms,
		targeting['exclusions']['net_worth'] as exclusions__net_worth,
		targeting['exclusions']['politics'] as exclusions__politics,
		targeting['exclusions']['relationship_statuses'] as exclusions__relationship_statuses,
		targeting['exclusions']['user_adclusters'] as exclusions__user_adclusters,
		targeting['exclusions']['work_employers'] as exclusions__work_employers,
		targeting['exclusions']['work_positions'] as exclusions__work_positions,
		targeting['facebook_positions'] as facebook_positions,
		targeting['family_statuses'] as family_statuses,
		targeting['flexible_spec'] as flexible_spec,
		targeting['friends_of_connections'] as friends_of_connections,
		targeting['genders'] as genders,
		targeting['generation'] as generation,
		targeting['geo_locations']['cities'] as geo_locations__cities,
		targeting['geo_locations']['countries'] as geo_locations__countries,
		targeting['geo_locations']['country_groups'] as geo_locations__country_groups,
		targeting['geo_locations']['custom_locations'] as geo_locations__custom_locations,
		targeting['geo_locations']['geo_markets'] as geo_locations__geo_markets,
		targeting['geo_locations']['location_types'] as geo_locations__location_types,
		targeting['geo_locations']['regions'] as geo_locations__regions,
		targeting['geo_locations']['zips'] as geo_locations__zips,
		targeting['home_ownership'] as home_ownership,
		targeting['home_type'] as home_type,
		targeting['income'] as income,
		targeting['industries'] as industries,
		targeting['instagram_positions'] as instagram_positions,
		targeting['interested_in'] as interested_in,
		targeting['interests'] as interests,
		targeting['locales'] as locales,
		targeting['messenger_positions'] as messenger_positions,
		targeting['moms'] as moms,
		targeting['net_worth'] as net_worth,
		targeting['office_type'] as office_type,
		targeting['politics'] as politics,
		targeting['publisher_platforms'] as publisher_platforms,
		targeting['relationship_statuses'] as relationship_statuses,
		targeting['targeting_optimization'] as targeting_optimization,
		targeting['user_adclusters'] as user_adclusters,
		targeting['user_device'] as user_device,
		targeting['user_os'] as user_os,
		targeting['work_positions'] as work_positions

    from source

)

select * from renamed
